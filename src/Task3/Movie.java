package Task3;
class Movie extends Media {
    public Movie(String title, int duration) {
        super(title, duration);
    }

    @Override
    public void play() {
        System.out.println("Playing movie: " + getTitle());
        System.out.println("Duration: " + getDuration() + " minutes");
        // Add movie-specific play logic here
    }
}

