package Task3;
public class Main {
    public static void main(String[] args) {
        Music music = new Music("Song Title", 4);
        music.play();

        System.out.println();

        Movie movie = new Movie("Movie Title", 120);
        movie.play();
    }
}
