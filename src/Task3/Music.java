package Task3;
class Music extends Media {
    public Music(String title, int duration) {
        super(title, duration);
    }

    @Override
    public void play() {
        System.out.println("Playing music: " + getTitle());
        System.out.println("Duration: " + getDuration() + " minutes");
        // Add music-specific play logic here
    }
}
