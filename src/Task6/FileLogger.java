package Task6;
public class FileLogger implements Logger {
    @Override
    public void logInfo(String message) {
        System.out.println("Info" + message);
    }

    @Override
    public void logWarning(String message) {
        System.out.println("Warning" + message);
    }

    @Override
    public void logError(String message) {
        System.out.println("Error" + message);
    }
}

