package Task6;

public interface Logger {
    void logInfo(String message);
    void  logWarning(String message);
    void logError(String message);

}

