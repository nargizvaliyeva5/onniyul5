public class Main {
    public static void main(String[] args) {
        Car car = new Car();

        // Set the car details
        car.setMake("Audi");
        car.setModel("RS7");
        car.setYear(2021);
        car.setRentalPrice(135.0);

        // Get and display the car details
        System.out.println("Car Details:");
        System.out.println("Make: " + car.getMake());
        System.out.println("Model: " + car.getModel());
        System.out.println("Year: " + car.getYear());
        System.out.println("Rental Price: $" + car.getRentalPrice());
    }
}
