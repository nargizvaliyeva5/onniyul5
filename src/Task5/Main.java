package Task5;

public class Main {
    public static void main(String[] args) {
        Dog dog =  new Dog();
        Cat cat =new Cat();
        Cow cow = new Cow();

        dog.makeSound();

        cat.makeSound();
        cow.makeSound();
    }
}

