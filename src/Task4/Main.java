package Task4;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Nargiz", 20, "S12345");
        Teacher teacher = new Teacher("Jale", 35, "Mathematics");

        student.displayInfo();
        System.out.println();
        teacher.displayInfo();
    }
}