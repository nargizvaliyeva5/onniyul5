package task2;
public class Audioplayer implements Playable  {
    @Override
    public void play() {
        System.out.println("Audio player is playing.");
    }

    @Override
    public void stop() {
        System.out.println("Audio player stopped.");
    }
}

