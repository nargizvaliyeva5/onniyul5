package task2;

import task2.Playable;

public class Videoplayer implements Playable {
    @Override
    public void play() {
        System.out.println("Video player is playing.");
    }

    @Override
    public void stop() {
        System.out.println("Video player stopped.");
}}
